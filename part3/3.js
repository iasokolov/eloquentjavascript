function countChar(_char) {
  return _string => {
    let count = 0;
    for (var i = 0; i < _string.length; i++) {
      if (_string[i] === _char) {
        count++;
      }
    }
    return count;
  }
}

const countBs = countChar('B');
console.log(countBs('qazwsxedcRFVBtgbB'));

const countCs = countChar('c');
console.log(countCs('qazwsxedcRFVBtgbB'));
