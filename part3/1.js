function getMin(a, b) {
  let ret = a;
  if (b < a) {
    ret = b;
  }
  return ret;
}

console.log(getMin(10, 5));
console.log(getMin(5, 10));
