function isEven(_value) {
  let ret;
  let value = Math.abs(_value);
  if (value == 0) {
    ret = true;
  } else if (value == 1) {
    ret = false;
  } else {
    ret = isEven(value - 2);
  }
  return ret;
}
console.log(isEven(50));
console.log(isEven(57));
console.log(isEven(-57));
