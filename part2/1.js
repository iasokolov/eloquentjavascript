console.log('Построение треугольника в цикле');
for (var i = 1; i <= 7; i++) {
  let line = '';
  do {
    line += '#';
  } while (line.length < i);
  console.log(`${i} - ${line}`);
}
