let size = 8;
let line3 = '';
console.log('Шахматная доска');
for (var i = 0; i < size; i++) {
  if (i % 2) {
    line3 += ` `;
  }
  for (var j = 0; j < size; j++) {
    if (j % 2) {
      line3 += `#`;
    }
    else {
      line3 += ` `;
    }
  }
  line3 += '\n';
}
console.log(line3);
